"""
WSGI config for projectwhatsapp project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append('/var/www/projectwhatsapp/')
sys.path.append('/var/www/projectwhatsapp/projectwhatsapp/')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'projectwhatsapp.settings')

application = get_wsgi_application()
