from django.contrib import admin
from . import models

class LogAdmin(admin.ModelAdmin):
    list_display = ("InvId", "OutSum")
    search_fields = ("InvId", "OutSum")

admin.site.register(models.SuccessNotification, LogAdmin)