from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from urllib.parse import unquote
from .models import Worker, Customer, Token, WGroup, Sites
import json
import requests
from .utils import send_mess_customer, add_to_group, send_mess_from_worker

def dialogs(request):
    if request.user.is_superuser:
        token_object = Token.objects.all()[0]
        dialogs = requests.get(token_object.url + '/dialogs?token=' + token_object.token)
        dialogs_dict = json.loads(dialogs.text)
        groups = ""

        for dialog in dialogs_dict['dialogs']:
            if dialog['metadata']['isGroup'] is True:
                groups = groups + dialog['id'] + "&nbsp" + "|" + "&nbsp" + dialog['name'] +  "&nbsp"  + "|" + "&nbsp" \
                         + str(len(dialog['metadata']['participants'])) + "<br>"

        return HttpResponse(groups)
    else:
        return HttpResponse(status=404)

@csrf_exempt
def worker(request, id):
    if request.method == 'POST':
        if Sites.objects.filter(id=id).exists():
            # wg = WGroup.objects.all()[0]
            body_unicode = request.body.decode('utf-8')
            body_unicode = unquote(body_unicode)
            body = {}
            body_split = body_unicode.split('&')
            for key in body_split:
                key_split = key.split('=')
                body[key_split[0]] = key_split[1]

            # print(body_unicode)
            # print(body)
            # body = json.loads(body_unicode)
            try:
                phone = body['Phone']
                if phone[0] == "+":
                    phone = phone[1:]

                if phone[0] == "8":
                    phone = "7" + phone[1:]

                name = body['Name'].replace('+', ' ')

                worker_ex = Worker.objects.filter(phone=phone).exists()

                if worker_ex is False:
                    mes = Worker(data=body_unicode, phone=phone, name=name)
                    mes.save()
                    send_mess_customer(mes)
                    add_to_group(mes.phone)
            except KeyError:
                print("KeyError")


            return HttpResponse(status=200)
        else:
            return HttpResponse(status=404)



    else:
        return HttpResponse(status=404)


@csrf_exempt
def customer(request, id):
    if request.method == 'POST':
        if Sites.objects.filter(id=id).exists():
            # form = FormForms(request.POST)
            # if form.is_valid():
            #     mess = Mess(data="")
            #
            # else:
            #     return HttpResponse(status=404)
            body_unicode = request.body.decode('utf-8')
            body_unicode = unquote(body_unicode)
            body = {}
            body_split = body_unicode.split('&')
            for key in body_split:
                key_split = key.split('=')
                body[key_split[0]] = key_split[1]

            # print(body_unicode)
            # print(body)
            # body = json.loads(body_unicode)
            try:
                phone = body['Phone']
                if phone[0] == "+":
                    phone = phone[1:]

                if phone[0] == "8":
                    phone = "7" + phone[1:]

                name = body['Name'].replace('+', ' ')
                mes = Customer(data=body_unicode, phone=phone, name=name)
                mes.save()
                text = body['Text'].replace('+', " ")
                send_mess_from_worker(text)
            except KeyError:
                print("KeyError")


            return HttpResponse(status=200)
        else:
            return HttpResponse(status=404)



    else:
        return HttpResponse(status=404)
