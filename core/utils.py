import json
import requests
from .models import Token, WGroup, Worker, Customer, Mess
from threading import Thread


def send_mess_to_chat(chatId, text):
    token_object = Token.objects.all()[0]

    reqeust_data = {
        'chatId': chatId,
        'body': text,

    }

    headers = {'Content-Type': 'application/json'}
    print(json.dumps(reqeust_data))
    r = requests.post(token_object.url + '/sendMessage?token=' + token_object.token, headers=headers,
                      data=json.dumps(reqeust_data))

    print(r.text)

    status = json.loads(r.text)

    return status


def send_mess(phone, name, token, url="https://eu24.chat-api.com/instance45789", text="Its work!"):
    text = text.replace("{{name}}", name)

    reqeust_data = {
        'phone': phone,
        'body': text,

    }

    headers = {'Content-Type': 'application/json'}
    print(json.dumps(reqeust_data))
    r = requests.post(url + '/sendMessage?token=' + token, headers=headers, data=json.dumps(reqeust_data))

    print(r.text)

    status = json.loads(r.text)

    return status


def send_mess_customer(mes):
    token_object = Token.objects.all()[0]

    status = send_mess(phone=mes.phone, name=mes.name, url=token_object.url, token=token_object.token,
                       text=token_object.text_worker)

    if status['sent'] is True:
        mes.mess = token_object.text_customers
        mes.save()
    else:
        mes.mess = str(status)
        mes.save()


def create_group(worker: Worker):
    wgs_count = WGroup.objects.all().count() + 1
    token_object = Token.objects.all()[0]
    body = {
        "groupName": "Группа " + str(wgs_count),
        "phones": [worker.phone],
        "messageText": "Добрый день, скоро с вами свяжутся!"

    }
    body_json = json.dumps(body)

    headers = {'Content-Type': 'application/json'}
    r = requests.post(token_object.url + '/group?token=' + token_object.token,
                      headers=headers, data=body_json)

    response = json.loads(r.text)
    print(response)
    try:
        if response['created'] == True:
            chatId=response['chatId']
            if chatId is None:
                dialogs = requests.get(token_object.url + '/dialogs?token=' + token_object.token)
                dialogs_dict = json.loads(dialogs.text)
                for dialog in dialogs_dict['dialogs']:
                    if dialog['metadata']['isGroup'] is True:
                        if dialog['name'] == "Группа " + str(wgs_count):
                            chatId=dialog['id']

            if chatId is not None:
                wg = WGroup(name="Группа " + str(wgs_count), groupId=chatId)
                wg.save()
                worker.wgroup = wg

            worker.save()

    except KeyError:
        pass


def _send_mess_from_worker(text):
    token_object = Token.objects.all()[0]
    wgs = WGroup.objects.all()
    for wg in wgs:
        send_mess_to_chat(wg.groupId, text)


def send_mess_from_worker(text):
    th1 = Thread(target=_send_mess_from_worker, args=(text,))
    th1.start()

    # if status['sent'] is True:
    #     mes.mess = token_object.text_customers
    #     mes.save()
    # else:
    #     mes.mess = str(status)
    #     mes.save()


def allgroup():
    token_object = Token.objects.all()[0]
    dialogs = requests.get(token_object.url + '/dialogs?token=' + token_object.token)
    dialogs_dict = json.loads(dialogs.text)
    groups = []
    for dialog in dialogs_dict['dialogs']:
        if dialog['metadata']['isGroup'] is True:
            if WGroup.objects.filter(groupId=dialog['id']).exists():
                group = {"id": dialog['id'], 'count': len(dialog['metadata']['participants'])}
                groups.append(group)

    return groups


def is_fullgroup(groups):
    for group in groups:
        if group['count'] < 255:
            return False
    return True


def _add_to_group_with_worker(worker: Worker):
    token_object = Token.objects.all()[0]
    groups = allgroup()
    print(allgroup())
    print(is_fullgroup(allgroup()))
    if is_fullgroup(groups):
        create_group(worker)
    else:
        for group in groups:
            if group['count'] < 255:
                wg = WGroup.objects.get(groupId=group['id'])

                reqeust_data = {
                    'participantPhone': worker.phone,
                    'groupId': wg.groupId

                }

                headers = {'Content-Type': 'application/json'}
                print(json.dumps(reqeust_data))
                r = requests.post(token_object.url + '/addGroupParticipant?token=' + token_object.token,
                                  headers=headers,
                                  data=json.dumps(reqeust_data))

                print(r.text)

                status = json.loads(r.text)
                try:
                    if status['add'] is True:
                        worker.wgroup = wg
                        worker.save()
                except:
                    pass

                return status


def _add_to_group(phone):
    token_object = Token.objects.all()[0]
    worker = Worker.objects.get(phone=phone)
    groups = allgroup()
    print(allgroup())
    print(is_fullgroup(allgroup()))
    if is_fullgroup(groups):
        create_group(worker)
    else:
        for group in groups:
            if group['count'] < 255:
                wg = WGroup.objects.get(groupId=group['id'])

                reqeust_data = {
                    'participantPhone': phone,
                    'groupId': wg.groupId

                }

                headers = {'Content-Type': 'application/json'}
                print(json.dumps(reqeust_data))
                r = requests.post(token_object.url + '/addGroupParticipant?token=' + token_object.token,
                                  headers=headers,
                                  data=json.dumps(reqeust_data))

                print(r.text)

                status = json.loads(r.text)
                try:
                    if status['add'] is True:
                        worker.wgroup = wg
                        worker.save()
                except:
                    pass

                return status


def add_to_group(phone):
    th1 = Thread(target=_add_to_group, args=(phone,))
    th1.start()


def send_mes_to_groups(text):
    groups = WGroup.objects.all()
    for group in groups:
        send_mess_to_chat(group.groupId, text)


def send_mes_to_users(text, model):
    token_object = Token.objects.all()[0]
    workers = model.objects.all()
    for worker in workers:
        send_mess(phone=worker.phone, name=worker.name, token=token_object.token, url=token_object.url, text=text)


def _send_mass_mess(mess: Mess):
    if mess.tomes == Mess.ALLGROUP:
        send_mes_to_groups(text=mess.text)
    if mess.tomes == Mess.ALLCUSTOMER:
        send_mes_to_users(text=mess.text, model=Customer)
    if mess.tomes == Mess.ALLWORKER:
        send_mes_to_users(text=mess.text, model=Worker)

def create_group_all(phones, text):
    wgs_count = WGroup.objects.all().count() + 1
    token_object = Token.objects.all()[0]
    body = {
        "groupName": "Группа " + str(wgs_count),
        "phones": phones,
        "messageText": "Добрый день, скоро с вами свяжутся!"

    }

    body_json = json.dumps(body)

    headers = {'Content-Type': 'application/json'}
    r = requests.post(token_object.url + '/group?token=' + token_object.token,
                      headers=headers, data=body_json)

    response = json.loads(r.text)
    print(response)
    try:
        if response['created'] == True:
            chatId = response['chatId']
            if chatId is None:
                dialogs = requests.get(token_object.url + '/dialogs?token=' + token_object.token)
                dialogs_dict = json.loads(dialogs.text)
                for dialog in dialogs_dict['dialogs']:
                    if dialog['metadata']['isGroup'] is True:
                        if dialog['name'] == "Группа " + str(wgs_count):
                            chatId = dialog['id']

            if chatId is not None:
                wg = WGroup(name="Группа " + str(wgs_count), groupId=chatId)
                wg.save()
                send_mess_to_chat(wg.groupId, text)
                workers_array = []
                for phone in phones:
                    workers_array.append(Worker(phone=phone, wgroup=wg))
                workers = Worker.objects.bulk_create(workers_array)
            else:
                print("Ну пиздец!")

    except KeyError:
        pass