from django.db import models
import random
import string
from django.db.models.signals import post_save
from django.dispatch import receiver
from threading import Thread
import re


# from core.logic import send_mass_mess

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


class WGroup(models.Model):
    groupId = models.TextField(primary_key=True, verbose_name="ID группы")
    name = models.TextField(verbose_name="Название группы")
    datetime = models.DateTimeField(auto_now_add=True, verbose_name="Время и дата")

    class Meta:
        verbose_name = "Группа Whatsapp"
        verbose_name_plural = "Группы Whatsapp"


class ImportNumber(models.Model):
    id = models.AutoField(primary_key=True)
    numbers = models.TextField(verbose_name="Номера через ;")
    message = models.TextField(blank=True, verbose_name="Сообщение")
    is_new_group = models.BooleanField(default=False, verbose_name="Попробовать добавить мгновенно")
    datetime = models.DateTimeField(auto_now_add=True, verbose_name="Время и дата")

    def __str__(self):
        return "Импорт номеров №" + str(self.id)

    class Meta:
        verbose_name = "Импорт номеров в группы"
        verbose_name_plural = "Импорт номеров в группы"



class Customer(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    data = models.TextField(verbose_name="Info from forms")
    phone = models.CharField(max_length=12, blank=True)
    text = models.TextField()
    datetime = models.DateTimeField(auto_now_add=True, verbose_name="Время и дата")

    class Meta:
        verbose_name = "Лид на работы(Заказчик)"
        verbose_name_plural = "Лиды на работы(Заказчики)"


class Token(models.Model):
    token = models.TextField(primary_key=True)
    url = models.URLField()
    text_customers = models.TextField(blank=True, verbose_name="Текст для заказчиков")
    text_worker = models.TextField(default="Привет {{name}}", verbose_name="Текст для работников")


class Worker(models.Model):
    id = models.AutoField(primary_key=True)
    mess = models.TextField(blank=True, verbose_name="Сообщение")
    data = models.TextField(verbose_name="Info from forms", blank=True)
    phone = models.CharField(max_length=12, verbose_name="Телефон")
    name = models.CharField(max_length=255, blank=True, verbose_name="Имя")
    wgroup = models.ForeignKey(WGroup, null=True, blank=True, default=None, on_delete=models.CASCADE,
                               verbose_name="В группе")
    datetime = models.DateTimeField(auto_now_add=True, verbose_name="Время и дата")

    class Meta:
        verbose_name = "Лид на работника"
        verbose_name_plural = "Лиды на работников"


class Sites(models.Model):
    id = models.CharField(max_length=11, default=randomString, primary_key=True)
    name = models.CharField(max_length=255, verbose_name="Имя сайта")

    class Meta:
        verbose_name = "Сайт"
        verbose_name_plural = "Сайты"


class Mess(models.Model):
    id = models.AutoField(primary_key=True)
    text = models.TextField(verbose_name='Текст')
    ALLWORKER = 'AW'
    ALLGROUP = 'AG'
    ALLCUSTOMER = 'AC'
    TO_SEND = [
        (ALLWORKER, 'Всем работникам'),
        (ALLGROUP, 'Во все группы'),
        (ALLCUSTOMER, 'Всем заказчикам'),
    ]
    tomes = models.CharField(choices=TO_SEND, verbose_name="Кому", max_length=3)

    def __str__(self):
        return self.text


    class Meta:
        verbose_name="Сообщение"
        verbose_name_plural="Сообщения"


from .utils import _send_mass_mess, send_mess, _add_to_group_with_worker, create_group_all



def send_mass_mess(mess):
    th1 = Thread(target=_send_mass_mess, args=(mess,))
    th1.start()

def _adds_num(instance):
    print(instance.numbers)
    numbers = re.sub("^\s+|\n|\r|\s+$", '', instance.numbers).split(';')
    print(numbers)
    token = Token.objects.all()[0]
    if instance.is_new_group is True:
        create_group_all(numbers, str(instance.message))
    else:
        for number in numbers:
            print(number)
            worker = Worker(phone=number)
            worker.save()
            _add_to_group_with_worker(worker)
    # send_mess(number, "", token.token, token.url, text=str(instance.message))


@receiver(post_save, sender=ImportNumber)
def adds_numbers(sender, instance,  **kwargs):
    th1 = Thread(target=_adds_num, args=(instance,))
    th1.start()



@receiver(post_save, sender=Mess, )
def update_stock(sender, instance, **kwargs):
    print("Its Work!")
    send_mass_mess(instance)
