# Generated by Django 2.2.2 on 2019-07-02 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20190702_1206'),
    ]

    operations = [
        migrations.AddField(
            model_name='importnumber',
            name='is_new_group',
            field=models.BooleanField(default=False, verbose_name='Попробовать добавить мгновенно'),
        ),
        migrations.AlterField(
            model_name='worker',
            name='phone',
            field=models.CharField(max_length=12, verbose_name='Телефон'),
        ),
    ]
