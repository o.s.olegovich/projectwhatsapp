from django.contrib import admin
from .models import Customer, Token, Worker, WGroup, Sites, Mess, ImportNumber


admin.site.site_header = 'Whatsapp project'  # default: "Django Administration"
admin.site.index_title = 'Админка'  # default: "Site administration"
admin.site.site_title = 'Whatsapp project | Админка'  # default: "Django site admin"

class WGAdmin(admin.ModelAdmin):
    list_display = ('name', 'groupId')
    list_display_links = ('name', 'groupId')


class WorkerAdmin(admin.ModelAdmin):
    list_display = ('phone', 'name', 'data')
    list_display_links = ('data', 'phone')
    search_fields = ('phone', 'name')


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('phone', 'name', 'data')
    list_display_links = ('data', 'phone')
    search_fields = ('phone', 'name')


class TokenAdmin(admin.ModelAdmin):
    list_display = ('token', 'url')

class SitesAdmin(admin.ModelAdmin):
    list_display = ('name', 'id')


class ImportNumberAdmin(admin.ModelAdmin):
    pass

class MessAdmin(admin.ModelAdmin):
    pass


admin.site.register(ImportNumber, ImportNumberAdmin)
admin.site.register(Mess, MessAdmin)
admin.site.register(Worker, WorkerAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Token, TokenAdmin)
admin.site.register(WGroup, WGAdmin)
admin.site.register(Sites, SitesAdmin)
